# *SRE service management scripts*
This is a collection of various scripts to help manage and troubleshoot
applications managed by Technology Operations.

## vipdb - Command line json parse utility for 'pos2' tool
> written and tested by Kevin Jones using python 2.6.6
>
> modules used: os, sys. json, getopt, requests, time, datetime
>
> http://pos2.np.wc1.yellowpages.com:9292/vip/.json

## smap - Command line json pare utility for opsdb services map endpoint
> written and tested by Kevin Jones using python 2.6.6
>
> modules used: os, sys. json, getopt, requests, time, datetime, PyYAML
>
> http://opsdb.int.yp.com/services/map.json

## lwes-listen - Command line lwes listener
> written and tested by Joshua Mervine
> contributed and modified by Kevin Jones using Ruby 1.9.3p551
>
> requires lwes, pry and prydoc
>
> https://github.com/lwes/

### Python Setup (not needed for docker container)

setup script will automatically peform the following actions. (sudo required)

1. install pip
2. install requests
3. install pyyaml
4. setup bash profile shortcuts (optional)

```
git clone git clone git@bitbucket.org:kj384g/sre-management.git $HOME/.sre
sh $HOME/.sre/setup.sh
```

### Bundler setup

```
gem install bundler
bundle install --path .bundle
```

### Bundler usage

```
bundle exec ./bin/listen.rb --help
```

execute from a different directory

```
BUNDLE_GEMFILE=$HOME/.sre/Gemfile bundle exec $HOME/.sre/bin/listen.rb --help
```

### Alias configuration
```
alias vipdb='python $HOME/.sre/bin/vip_query.py'
alias smap='python $HOME/.sre/bin/opsdb_services_map.py'
alias lwes-listen='BUNDLE_GEMFILE=$HOME/.sre/Gemfile bundle exec $HOME/.sre/bin/lwes_parse.rb'
```

### Usage

opsdb services map - command line json parse utility for opsdb

```
usage: smap [options]
         -e/--environment       <environment>
         -s/--service           <service>
         -d/--datacenter        <datacenter>
         -o/--output            </path/to/file>
         -r                     displays opsdb services map in raw json
         -l                     displays all available services
         -c                     displays config.yaml entry for specific service
         -t                     perform basic test - service/env argument required
                                 (only first url will be tested)
         -v                     perform advanced/verbose test - service/env argument required
                                 (all urls defined in configuration will be tested)
         -u/--url               <custom test url>
         -h                     displays this help menu
```

vipdb - command line json parse utility for "pos2" tool

```
usage: vipdb [options]
         -e/--environment        <environment>
         -s/--service            <service>
         -d/--datacenter         <datacenter>
         -p/--port               <port>
         -o/--output             </path/to/file>
         -c                      provides on fly vip healthcheck
         -r                      displays raw json output
         -m                      displays pool_members only
         -h                      displays this help menu
```

lwes-listen - lwes listener

```
 usage: lwes_parse.rb [options]
    -p, --port [PORT]                lwes listener port
    -a, --address [ADDRESS]          lwes listener address
    -o, --output [FILE]              output events to file
    -x, --aid [AID]                  filter by aid
    -c, --code [CODE]                filter by code
    -i, --inreq [INREQ]              filter by inreq
    -q, --quiet                      do not print STDOUT
    -h, --help                       display this help message


 examples:
 basic usage for filtering a specific aid and status code
 $ lwes-listen --aid bento --code 500

 filter for specific requests being made using the inreq flag
 $ lwes-listen --aid m.yp.com --inreq googlebot

 regex patterns are also accepted for lwes filters
 $ lwes-listen --aid m.yp.com --code 30[0-9]
 $ lwes-listen --aid "(bento|firefly)" --code 200
 $ lwes-listen --aid ypu --inreq "(googlebot|bingbot)"

 listen on specific address and ports
 $ lwes-listen -a 224.1.2.22 -p 1222
```

## Docker usage

### smap - opsdb services map

-run interactive mode (attach)

```
sudo docker pull 'docker-dev.yp.com/kj384g/sre-opsdb-smap-python:latest'
sudo docker run -it --rm docker-dev.yp.com/kj384g/sre-opsdb-smap-python:latest /bin/bash
```

-run via shell

```
sudo docker run -it --rm 'docker-dev.yp.com/kj384g/sre-opsdb-smap-python:latest' python /usr/src/app/bin/opsdb_services_map.py -h
```
