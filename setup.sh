# setup script for vip_request.py

pip_url='https://bootstrap.pypa.io/get-pip.py'
pyaml_url='http://pyyaml.org/download/pyyaml/PyYAML-3.11.tar.gz'

# here we download the newest version of pip
wget --no-check-certificate $pip_url -P /tmp/
sudo python /tmp/get-pip.py

# here we install PyYAML
mkdir $HOME/.sre/tmp

wget $pyaml_url -P $HOME/.sre/tmp/
tar -xzvf $HOME/.sre/tmp/PyYAML-3.11.tar.gz -C $HOME/.sre/tmp

sudo python $HOME/.sre/tmp/PyYAML-3.11/setup.py install

# here we install requests for python
sudo pip install requests

# alias creation
alias vipdb='python $HOME/.sre/bin/vip_query.py'
alias smap='python $HOME/.sre/bin/opsdb_services_map.py'
alias lwes-listen='BUNDLE_GEMFILE=$HOME/.sre/Gemfile bundle exec $HOME/.sre/bin/listen.rb'

read -p "Add shortcut aliases to bash profile? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo
  echo "creating alias vipdb='python $HOME/.sre/bin/vip_query.py'"
  echo "alias vipdb='python $HOME/.sre/bin/vip_query.py'" >> $HOME/.bash_profile
  echo ""
  echo "creating alias smap='python $HOME/.sre/bin/opsdb_services_map.py'"
  echo "alias smap='python $HOME/.sre/bin/opsdb_services_map.py'" >> $HOME/.bash_profile
  echo ""
  echo "creating alias lwes-listen='BUNDLE_GEMFILE=$HOME/.sre/Gemfile bundle exec $HOME/.sre/bin/listen.rb'"
  echo "alias lwes-listen='BUNDLE_GEMFILE=$HOME/.sre/Gemfile bundle exec $HOME/.sre/bin/listen.rb'" >> $HOME/.bash_profile
  source $HOME/.bash_profile
fi

echo

if [ -f $HOME/.sre/bin/vip_query.py ]; then
  echo 'vipdb installed use vipdb -h for usage'
  echo
fi

if [ -f $HOME/.sre/bin/opsdb_services_map.py ]; then
  echo 'smap installed use smap -h for usage'\n
  echo
fi

if [ -f $HOME/.sre/bin/lwes_parse.rb ]; then
  echo 'lwes-listen installed lwes-listen -h for usage'\n
  echo
fi
