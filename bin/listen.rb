#!/usr/bin/env ruby
trap("INT") do
  output.close rescue nil
  puts
  puts "~"*80
  exit
end

require 'optparse'

me      = File.basename(__FILE__)
options = { :address => "localhost", :port => 12345 }

OptionParser.new do |opts|
  opts.banner = "Usage: #{me} [options]"

  opts.on("-p", "--port [PORT]", "Listener port.") do |p|
    options[:port] = p
  end

  opts.on("-a", "--address [ADDRESS]", "Listener address.") do |a|
    options[:address] = a
  end

  opts.on("-o", "--output [FILE]", "Tee's events to file.") do |o|
    options[:output] = o
  end

  opts.on("-f", "--filter [REGEX]", "Regex to filter on.") do |r|
    options[:filter] = Regexp.new(r)
  end

  opts.on("-q", "--quiet", "Do not print to STDOUT.") do
    options[:quiet] = true
  end

  opts.on("-h", "--help", "Display this help message.") do
    puts opts
    exit
  end

  opts.separator ""
  opts.separator "Note on the filter option:"
  opts.separator ""
  opts.separator "This filters on event.to_hash.to_s, so keep that"
  opts.separator "mind when creating your regex."
  opts.separator ""
  opts.separator "  Examples:"
  opts.separator ""
  opts.separator "  --filter ':aid=>\"webyp\"'"
  opts.separator "  --filter '^{:name=>\"YP::Mon::Perf\".*:aid=>\"webyp\"'"
  opts.separator ""


end.parse!

require 'lwes'
listener = LWES::Listener.new :address => options[:address].to_s, :port => options[:port].to_i
output = File.open(options[:output], 'a') if options.has_key?(:output)

puts "Starting LWES Listener on #{options[:address]}:#{options[:port]}."
puts "-> Filtering on: #{options[:filter]}" if options.has_key?(:filter)
puts "~"*80
require 'pp'
begin
  listener.each do |event|
    next if options.has_key?(:filter) && event.to_hash.to_s !~ options[:filter]
    pp event.to_hash unless options.has_key?(:quiet)

    if options.has_key?(:output)
      output.puts event.to_hash
      output.flush
    end
  end
rescue RuntimeError => e
  $stderr.puts "ERROR: "+e.message
  $stderr.flush
  retry
end

# vim: filetype=ruby:
