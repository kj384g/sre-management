#!/usr/bin/python

#   written and tested by Kevin Jones using python 2.6.6
#
#   requires below imports, recommended to use pip to install modules
#   https://pip.pypa.io/en/latest/installing.html for more information
#
#   python-requests install
#   http://docs.python-requests.org/en/latest/user/install/

import os
import sys
import json
import yaml
import getopt
import pprint
import requests
import tempfile
import time
import datetime

#   disable ssl warning
requests.packages.urllib3.disable_warnings()

#   define api variables
uri = 'http://opsdb.flight.yellowpages.com/services/map'
xml_node_uri = 'http://opsdb.wc1.yellowpages.com/nodes.xml?name='

#   define configuration file
home = os.getenv("HOME")
conf_yaml = home+'/.sre/config.yaml'

#   define server time variables
ts = time.time()
servertime = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
serverhour = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H%M%S')
serverdate = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')

#   define pretty print with indent
pp = pprint.PrettyPrinter(indent=2)

#   here we create a temporary file to store the initial json response
#   http://pymotw.com/2/tempfile/

#temp_file = tempfile.NamedTemporaryFile()

#   debugging to show the temp_file name and attributes after closing it
#try:
#    print 'temp:', temp_file
#    print 'temp.name:', temp_file.name
#finally:
#    temp_file.close()
#    print 'Exists after close:', os.path.exists(temp_file.name)

#   debugging to show arguments length and arguments string using sys

    #print 'Number of arguments:', len(sys.argv), 'arguments.'
    #print 'Argument List:', str(sys.argv)

#   here we define the command line switches for the arguments

def main(argv):
    environment = ''
    service = ''
    datacenter =''
    output = ''
    test_config = ''
    build = ''
    raw = False
    download = False
    list_serv = False
    test = False
    verbose = False
    show_config = False
    load_config = False
    build_config = False
    sort_config = False
    custom_url = ''
    custom_test = False
    try:
        opts, args = getopt.getopt(argv,"hvtclre:s:d:o:b:u:",["environment=","service=","datacenter=","output=","build=","url="])
    except getopt.GetoptError:
        print 'opsdb service map - command line json parse utility for opsdb'
        print 'usage:'
        print '\t -e/--environment \t<environment>'
        print '\t -s/--service \t\t<service>'
        print '\t -d/--datacenter \t<datacenter>'
        print '\t -o/--output\t\t</path/to/file>'
        print '\t -r\t\t\tdisplays opsdb services map in raw json'
        print '\t -l\t\t\tdisplays all available services'
        print '\t -c\t\t\tdisplays config.yaml entry for specific service'
        print '\t -t\t\t\tperform basic test - service/env argument required'
        print '\t\t\t\t (only first url will be tested)'
        print '\t -v\t\t\tperform advanced/verbose test - service/env argument required'
        print '\t\t\t\t (all urls defined in configuration will be tested)'
        print '\t -u/--url\t\t<custom test url>'
        print '\t -h\t\t\tdisplays this help menu'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'opsdb service map - command line json parse utility for opsdb'
            print 'usage:'
            print '\t -e/--environment \t<environment>'
            print '\t -s/--service \t\t<service>'
            print '\t -d/--datacenter \t<datacenter>'
            print '\t -o/--output\t\t</path/to/file>'
            print '\t -r\t\t\tdisplays opsdb services map in raw json'
            print '\t -l\t\t\tdisplays all available services'
            print '\t -c\t\t\tdisplays config.yaml entry for specific service'
            print '\t -t\t\t\tperform basic test - service/env argument required'
            print '\t\t\t\t (only first url will be tested)'
            print '\t -v\t\t\tperform advanced/verbose test - service/env argument required'
            print '\t\t\t\t (all urls defined in configuration will be tested)'
            print '\t -u/--url\t\t<custom test url>'
            print '\t -h\t\t\tdisplays this help menu'
            sys.exit()
        elif opt == ("-v"):
            load_config = True
            test = True
            verbose = True
        elif opt == ("-t"):
            load_config = True
            test = True
        elif opt == ("-c"):
            load_config = True
            show_config = True
        elif opt == ("-l"):
            sort_config = True
            list_serv = True
        elif opt == ("-r"):
            raw = True
        elif opt in ("-e", "--environment"):
            environment = arg
        elif opt in ("-s", "--service"):
            service = arg
        elif opt in ("-d", "--datacenter"):
            datacenter = arg
        elif opt in ("-o", "--output"):
            output = arg
            download = True
        elif opt in ("-b", "--build"):
            build_config = True
            sort_config = True
            build = arg
        elif opt in ("-u", "--url"):
            custom_url = arg
            custom_test = True

    if environment == 'nca':
        environment = 'prod'
    elif environment == '':
        environment = 'all'
    else:
        environment = environment

#   validate the datacenter is valid and set to all if neccesary

    if datacenter == '':
        datacenter = 'all'
    elif datacenter == 'ev1':
        datacenter = datacenter
    elif datacenter == 'wc1':
        datacenter = datacenter
    else:
        print 'unknown data center detected.'
        sys.exit(1)

#   here we can check for missing user entries

    if custom_test == True:
        if test == False:
            print 'please specify -t or -v to test a custom url'
            sys.exit(1)

#   here we query opsdb for the full services map

    response = requests.get(uri)
    results = response.json()

    if response.status_code == 200:
        success = True
    elif response.status_code == 404:
        print 'status_code:', response.status_code, ' - sorry no results found'
        sys.exit(1)
    elif response.status_code == 504:
        print 'status_code:', response.status_code, ' - gateway timeout'
        sys.exit(1)
    elif response.status_code == 500:
        print 'status_code:', response.status_code, ' - internal server error'
        sys.exit(1)
    elif response.status_code == 400:
        print 'status_code:', response.status_code, ' - bad request'
        sys.exit(1)
    elif response.status_code == 401:
        print 'status_code:', response.status_code, ' - unauthorized'
        sys.exit(1)
    elif response.status_code == 403:
        print 'status_code:', response.status_code, ' - forbidden'
        sys.exit(1)
    elif response.status_code != 200:
        print 'sorry something went wrong'
        sys.exit(1)

    if raw == True:
        #print "processing...\n"
        print(json.dumps(results, indent=4))
        if download == True:
            with open(output, 'wb') as fd:
                for chunk in response.iter_content(1024):
                    fd.write(chunk)
                    fd.flush
                print 'writing results to...', output
                sys.exit()
        else:
            sys.exit()

    if download == True:
        with open(output, 'wb') as fd:
            for chunk in response.iter_content(1024):
                fd.write(chunk)
                fd.flush()
            print 'writing results to...', output
            sys.exit()

    #if list_serv == True:
        #for nodegroup in results:
            #print nodegroup
        #sys.exit()

#   here we load the yaml data for the configuration file

    if load_config == True:
        with open(conf_yaml, 'r') as data:
            test_config = yaml.load(data)
            data.close()

#   here we first grab all of the services and then create a list

    if sort_config == True:
        ng_list = []
        for nodegroup in results:
            ng_list.append(nodegroup)

#   here we print out the available services

    if list_serv == True:
        for ng in sorted(ng_list):
            print ng
        sys.exit()

#   here we loop through to test the config

    if test == True:
        for nodegroup in results:
            if nodegroup == service:
                smoke_service = nodegroup
                for env in results[nodegroup]:
                    if environment == env:
                        for dc in results[nodegroup][env]:
                            if datacenter == dc:
                                for host in results[nodegroup][env][dc]:
                                    for aid in test_config:
                                        if smoke_service == aid['service']:
                                            print 'hostname:', host
                                            print 'port:', aid['port']
                                            if aid['type'] == 'non-ssl':
                                                hyper = 'http://'
                                            elif aid['type'] == 'ssl':
                                                hyper = 'https://'
                                            if verbose == True:
                                                if custom_test == True:
                                                    smoke_url = str(hyper)+str(host)+':'+str(aid['port'])+str(custom_url)
                                                    verbose_test(smoke_url)
                                                elif custom_test == False:
                                                    for test_url in aid['service_tests']:
                                                        if not test_url:
                                                            print 'no test configuration defined in yaml'
                                                        smoke_url = str(hyper)+str(host)+':'+str(aid['port'])+str(test_url['test_url'])
                                                        verbose_test(smoke_url)
                                            elif verbose == False:
                                                if custom_test == True:
                                                    smoke_timeout = aid['timeout']
                                                    smoke_url = str(hyper)+str(host)+':'+str(aid['port'])+str(custom_url)
                                                    smoke_test(smoke_url)
                                                elif custom_test == False:
                                                    test_url = aid['service_tests'][0]
                                                    basic = test_url['test_url']
                                                    smoke_timeout = aid['timeout']
                                                    smoke_url = str(hyper)+str(host)+':'+str(aid['port'])+str(basic)
                                                    smoke_test(smoke_url)
                            elif datacenter == "all":
                                for host in results[nodegroup][env][dc]:
                                    for aid in test_config:
                                        if smoke_service == aid['service']:
                                            print 'hostname:', host
                                            print 'port:', aid['port']
                                            if aid['type'] == 'non-ssl':
                                                hyper = 'http://'
                                            elif aid['type'] == 'ssl':
                                                hyper = 'https://'
                                            if verbose == True:
                                                if custom_test == True:
                                                    smoke_timeout = aid['timeout']
                                                    smoke_url = str(hyper)+str(host)+':'+str(aid['port'])+str(custom_url)
                                                    verbose_test(smoke_url)
                                                elif custom_test == False:
                                                    for test_url in aid['service_tests']:
                                                        if not test_url:
                                                            print 'no test configuration defined in yaml'
                                                        smoke_timeout = aid['timeout']
                                                        smoke_url = str(hyper)+str(host)+':'+str(aid['port'])+str(test_url['test_url'])
                                                        verbose_test(smoke_url)
                                            elif verbose == False:
                                                if custom_test == True:
                                                    smoke_timeout = aid['timeout']
                                                    smoke_url = str(hyper)+str(host)+':'+str(aid['port'])+str(custom_url)
                                                    smoke_test(smoke_url)
                                                elif custom_test == False:
                                                    test_url = aid['service_tests'][0]
                                                    basic = test_url['test_url']
                                                    smoke_timeout = aid['timeout']
                                                    smoke_url = str(hyper)+str(host)+':'+str(aid['port'])+str(basic)
                                                    smoke_test(smoke_url)
        sys.exit()

#   here we loop through and print out the requested yaml configuration

    if show_config == True:
        for nodegroup in results:
            if service == nodegroup:
                for aid in test_config:
                    if aid['service'] == service:
                        print '---'
                        print '- service: '+aid['service']
                        print '  info: '+aid['info']
                        print '  type: '+aid['type']
                        print '  port: '+str(aid['port'])
                        print '  header: '+aid['header']
                        print '  timeout: '+str(aid['timeout'])
                        print '  service_tests:'
                        for test_url in aid['service_tests']:
                            print '  - test_url: '+test_url['test_url']
        sys.exit()

#   now we loop through the list and build a yaml template

    if build_config == True:
        template = open(build, "w")
        sys.stdout = template
        print '---'
        for ng in sorted(ng_list):
            print '- service: '+ng
            print '  info: '
            print '  type: '
            print '  type: '
            print '  port: '
            print '  header: '
            print '  timeout: 30'
            print '  service_tests:'
            print '  - test_url: '
            print '  - test_url: '
            print '  - test_url: '
        sys.exit()

#   here we loop through and give data for all exact matches / or partial

    for nodegroup in results:
        if nodegroup == service:
            for env in results[nodegroup]:
                if environment == env:
                    for dc in results[nodegroup][env]:
                        if datacenter == dc:
                            for host in results[nodegroup][env][dc]:
                                print host
                        elif datacenter == "all":
                            for host in results[nodegroup][env][dc]:
                                print host
                elif environment == 'all':
                    for dc in results[nodegroup][env]:
                        if datacenter == dc:
                            for host in results[nodegroup][env][dc]:
                                print host
                        elif datacenter == 'all':
                            for host in results[nodegroup][env][dc]:
                                print host
            sys.exit()
        elif service in nodegroup:
            print 'service:', nodegroup
            for env in results[nodegroup]:
                if environment == env:
                    for dc in results[nodegroup][env]:
                        if datacenter == dc:
                            for host in results[nodegroup][env][dc]:
                                print ' - '+host
                        elif datacenter == "all":
                            for host in results[nodegroup][env][dc]:
                                print ' - '+host
                elif environment == 'all':
                    for dc in results[nodegroup][env]:
                        if datacenter == dc:
                            for host in results[nodegroup][env][dc]:
                                print ' - '+host
                        elif datacenter == 'all':
                            for host in results[nodegroup][env][dc]:
                                print ' - '+host

                    #else:
                        #print environment
                        #print 'environment not recognized'
                        #sys.exit()
#   pretty print the data
#   https://docs.python.org/2/library/pprint.html

    #pp.pprint(results).json()

def smoke_test( str ):
    try:
        smoke_val = requests.get(str, timeout=30, verify=False);
        #print ' url:', smoke_val.url
        print ' status_code:', smoke_val.status_code
        #pp.pprint(smoke_val.headers)
        print ' elapsed:', smoke_val.elapsed, '\n'
    except requests.exceptions.RequestException as e:
        print bcolors.FAIL + ' error:', e, bcolors.ENDC, '\n'
    return;

def verbose_test( str ):
    try:
        smoke_val = requests.get(str, timeout=30, verify=False);
        print 'content:', smoke_val.content
        print 'url:', smoke_val.url
        print 'status_code:', smoke_val.status_code
        print 'headers:', smoke_val.headers
        print 'elapsed:', smoke_val.elapsed
        print 'history:', smoke_val.history
        print 'links:', smoke_val.links
    except requests.exceptions.RequestException as e:
        print bcolors.FAIL + 'error:', e, bcolors.ENDC
    return;

def auth_test( str ):
    try:
        user = 'kj384g'
        passwd = requests.get('under_construction')
        smoke_val = requests.get(str, timeout=30, verify=False, auth=HTTPBasicAuth(user, passwd));
        #print ' url:', smoke_val.url
        print ' status_code:', smoke_val.status_code
        #pp.pprint(smoke_val.headers)
        print ' elapsed:', smoke_val.elapsed, '\n'
    except requests.exceptions.RequestException as e:
        print bcolors.FAIL + ' error:', e, bcolors.ENDC, '\n'
    return;

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

if __name__ == "__main__":
    main(sys.argv[1:])
