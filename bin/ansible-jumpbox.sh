#!/bin/sh

tag="$1"
test "$1" || tag="latest"

docker="sudo docker"
if [[ "$(uname)" = "Darwin" ]]; then
  docker="docker"

  if [[ "$(boot2docker status)" != "running" ]]; then
    echo "+ boot2docker up"
    $(boot2docker up | grep "export DOCKER_")
  fi
fi

image="docker-dev.yp.com/appsupport/ansible-jumpbox:${tag}"

set -uex
${docker} pull ${image}
${docker} run --rm -it -e USER=${USER} ${image}
