require 'yaml'
require 'nventory'

@hosts = File.open(ARGV.shift).read.split("\n")

nvclient = NVentory::Client.new(:dryrun => true, :debug => false, :server => 'http://opsdb.flight.yellowpages.com')
nvquery = {:get => {:name => @hosts } ,:objecttype => "nodes", :includes => (['node_groups[tags]','operating_system','hardware_profile']) }

opsdb_results = nvclient.get_objects(nvquery)

my_output = []
opsdb_results.each do |node, node_data|
   n = {
    :name => node,
    :hw => "",
    :os => "",
    # since these are tagged nodegroups, there is potential to have multiple values
    :datacenter => [],
    :environment => [],
    :services => [],
    :tier => [],
    :cluster => [],
    :other_ng => [],
  }
  n[:os] = node_data["operating_system"]["name"] rescue nil
  n[:hw] = node_data["hardware_profile"]["name"] rescue nil

  node_data["node_groups"].each do |ng|
    if ng["tags"].any?
      puts ng["tags"].to_yaml
      ng["tags"].each do |tag|
        tag_name = tag["name"].to_sym
        if n.key?(tag_name)
          n[tag_name] << ng["name"]
        end
      end
    else
      n[:other_ng] << ng["name"]
    end
  end
  my_output << n
end

# can specify specific fields to dump in csv
fields = %w|name os hw environment services tier cluster|
# or grab all fields
fields = my_output[0].keys

my_output.each do |node|
  line = []
  fields.each do |f|
    fv = node[f.to_sym]
    value = (fv.is_a?(Array)) ? fv.join(',') : fv
    line << value
  end
  puts line.collect{|v| "\"#{v}\""}.join(",")
end
