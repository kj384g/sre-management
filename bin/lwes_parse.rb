#!/usr/bin/env ruby

require 'optparse'
require 'lwes'
require 'pp'

trap("INT") do
  output.close rescue nil
  exit
end

# here we set the variables needed for the script

rb      = File.basename(__FILE__)
options = { :address => "224.1.2.22", :port => 1222 }

# here we use optparse to grab all the user defined variables and flags

OptionParser.new do |opts|
  opts.banner = " usage: #{rb} [options]"

  opts.on("-p", "--port [PORT]", "lwes listener port") do |p|
    options[:port] = p
  end

  opts.on("-a", "--address [ADDRESS]", "lwes listener address") do |a|
    options[:address] = a
  end

  opts.on("-o", "--output [FILE]", "output events to file") do |o|
    options[:output] = o
  end

  opts.on("-x", "--aid [AID]", "filter by aid") do |x|
    x = ":aid=>\"" + x + "\""
    options[:aid] = Regexp.new(x)
  end

  opts.on("-c", "--code [CODE]", "filter by aid") do |c|
    c = ":code=>" + c
    options[:code] = Regexp.new(c)
  end

  opts.on("-i", "--inreq [INREQ]", "filter by inreq") do |i|
    c = ":inreq=>\"" + i + "\""
    options[:inreq] = Regexp.new(i)
  end

  opts.on("-q", "--quiet", "do not print STDOUT") do
    options[:quiet] = True
  end

  opts.on("-h", "--help", "display this help message") do
    puts opts
    exit 0
  end

  opts.separator ""
  opts.separator ""
  opts.separator " examples:"
  opts.separator " basic usage for filtering a specific aid and status code"
  opts.separator " $ lwes-listen --aid bento --code 500"
  opts.separator ""
  opts.separator " filter for specific requests being made using the inreq flag and output to file"
  opts.separator " $ lwes-listen --aid m.yp.com --inreq googlebot"
  opts.separator ""
  opts.separator " regex patterns are also accepted for lwes filters"
  opts.separator " $ lwes-listen --aid m.yp.com --code 30[0-9]"
  opts.separator " $ lwes-listen --aid \"(bento|firefly)\" --code 200"
  opts.separator " $ lwes-listen --aid ypu --inreq \"(googlebot|bingbot)\""
  opts.separator ""
  opts.separator " listen on specific address and ports"
  opts.separator " $ lwes-listen -a 224.1.2.22 -p 1222"
  opts.separator ""

end.parse!

# here we start invoking the lwes listener to grab data

listener = LWES::Listener.new :address => options[:address].to_s, :port => options[:port].to_i

# here we open the file to save to if output option is used

output = File.open(options[:output], 'a') if options.has_key?(:output)

puts "Listening for LWES events on #{options[:address]}:#{options[:port]}"
puts " - aid: #{options[:aid]}" if options.has_key?(:aid)
puts " - code: #{options[:code]}" if options.has_key?(:code)
puts " - inreq: #{options[:inreq]}" if options.has_key?(:inreq)

begin
  listener.each do |event|
    next if options.has_key?(:aid) && event.to_hash.to_s !~ options[:aid]
    next if options.has_key?(:code) && event.to_hash.to_s !~ options[:code]
    next if options.has_key?(:inreq) && event.to_hash.to_s !~ options[:inreq]

    pp event.to_hash unless options.has_key?(:quiet)

    if options.has_key?(:output)
      output.puts event.to_hash
      output.flush
    end
  end

rescue RuntimeError => e
  $stderr.puts "error: "+e.message
  $stderr.flush
  retry
end
