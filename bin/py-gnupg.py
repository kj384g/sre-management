import os
import gnupg
import getpass
import getopt
import sys
import time
import datetime
from pprint import pprint

# define some needed system variables
home = os.getenv("HOME")
ts = time.time()
stamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H%M%S')

# here we define the gpg variables
gpg = gnupg.GPG(gnupghome=home+'/pygpg')
gpg.encoding = 'utf-8'

# export variables
gpg_pubkey = home+'/mypubkey.asc'
gpg_prikey = home+'/myprikey.asc'
gpg_pubqr = home+'/mypubkey.png'
gpg_priqr = home+'/myprikey.png'
gpg_pubexp = home+'/mypubkey'+stamp+'.asc'
gpg_priexp = home+'/myprikey'+stamp+'.asc'

# main arguments
def main(argv):
    exp_keys = False
    gen_keys = False
    gen_qrs = False
    fencrypt = False
    fdecrypt = False
    enc_sig = ''
    import_key = False
    export_key = False
    list_keys = False
    shred_keys = False
    enc_file = ''
    dec_file = ''
    key_file = ''
    try:
        opts, args = getopt.getopt(argv, "hgkqlse:d:u:i:x:", ["encrypt-file=","decrypt-file=","uid-email=","import-key=","export-key="])
    except getopt.GetoptError:
        print 'pygnupg - python gnupg wrapper'
        print 'use -h for help menu'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'pygnupg - python gnupg wrapper'
            print 'usage:'
            print '\t -e/--encrypt <file>\t\t\tencrypt a file'
            print '\t -d/--decrypt <file>\t\t\tdecrypt a file'
            print '\t -u/--uid-email <email>\t\t\tspecify uid email'
            print '\t -i/--import-key <file>\t\t\timport a gpg key'
            print '\t -x/--export-key <search string>\texport a gpg key'
            print '\t -l\t\t\tlist all gpg keys in keystore'
            print '\t -g\t\t\tgenerate a gpg key pair'
            print '\t -k\t\t\texport pub/private key files during keygen [.asc]'
            print '\t -q\t\t\texport pub/private qr codes images during keygen [.png]'
            print '\t -s\t\t\tshred pygpg directory [WARNING: destructive command!]'
            print '\t -h\t\t\tdisplays this help menu'
            sys.exit()
        elif opt == ("-s"):
            shred_keys = True
        elif opt == ("-l"):
            list_keys = True
        elif opt == ("-g"):
            gen_keys = True
        elif opt == ("-k"):
            exp_keys = True
        elif opt == ("-q"):
            gen_qrs = True
        elif opt in ("-e", "--encrypt-file"):
            fencrypt = True
            enc_file = arg
        elif opt in ("-d", "--decrypt-file"):
            fdecrypt = True
            dec_file = arg
        elif opt in ("-u", "--uid-email"):
            enc_sig = arg
        elif opt in ("-i", "--import-key"):
            import_key = True
            keyfile = arg
        elif opt in ("-x", "--export-key"):
            export_key = True
            key_search = arg

    if shred_keys == True:
        destructive = raw_input('Are you sure you want to shred your pygpg directory? (type yes or no) ')
        if destructive == 'yes':
            os.system('shred -f -n 10 -v -u '+home+'/pygpg/*')
        else:
            sys.exit()

    if gen_keys == True:

        # here we accept the email and password to generate a key
        gpg_email = raw_input('Enter an email address: ')
        gpg_pass = getpass.getpass()

        # here we generate a key pair based on email and passphrase
        input_data = gpg.gen_key_input(
            name_email=str(gpg_email),
            passphrase=str(gpg_pass))
        key = gpg.gen_key(input_data)

        print key

        if exp_keys == True:

            # here we export the key to a file
            ascii_armored_public_keys = gpg.export_keys(str(key))
            with open(gpg_pubkey, 'w') as f:
                f.write(ascii_armored_public_keys)
            print '...file written to '+gpg_pubkey

            ascii_armored_private_keys = gpg.export_keys(str(key), True)
            with open(gpg_prikey, 'w') as f:
                f.write(ascii_armored_private_keys)
            print '...file written to '+gpg_prikey

        if gen_qrs == True:

            if exp_keys == False:
                print 'Generate asc files to use this function'
            else:
                # here we export the keyfile to a qr code file
                os.system('qrencode -o '+gpg_pubqr+' < '+gpg_pubkey)
                print '...file written to '+gpg_pubqr
                os.system('qrencode -o '+gpg_priqr+' < '+gpg_prikey)
                print '...file written to '+gpg_priqr

    if import_key == True:

        # here we import the keys
        key_data = open(keyfile).read()
        import_result = gpg.import_keys(key_data)
        pprint(import_result.results)

    if export_key == True:

        ascii_armored_public_keys = gpg.export_keys(key_search, minimal=True)
        with open(gpg_pubexp, 'w') as f:
            f.write(ascii_armored_public_keys)
        print '...file written to '+gpg_pubexp

        ascii_armored_private_keys = gpg.export_keys(key_search, True, minimal=True)
        with open(gpg_priexp, 'w') as f:
            f.write(ascii_armored_private_keys)
        print '...file written to '+gpg_priexp

    if list_keys == True:

        # here we list out the keys
        public_keys = gpg.list_keys()
        private_keys = gpg.list_keys(True)
        print 'public keys:'
        pprint(public_keys)
        print 'private keys:'
        pprint(private_keys)

    if fencrypt == True:
        if enc_sig == '':
            enc_sig = raw_input('Enter an email address: ')
        else:
            # here we encrypt a file
            efile = open(enc_file).read()
            encrypted_data = gpg.encrypt(efile, enc_sig)
            encrypted_string = str(encrypted_data)
            print 'ok: ', encrypted_data.ok
            print 'status: ', encrypted_data.status
            print 'stderr: ', encrypted_data.stderr
            print 'encrypted_string: ', encrypted_string
            with open(enc_file+'.gpg', 'w') as f:
                f.write(encrypted_string)
            print '...file written to '+enc_file+'.gpg'

    if fdecrypt == True:

        dec_pass = getpass.getpass()

        fileName, fileExtension = os.path.splitext(dec_file)

        # here we can decrypt a file
        with open(dec_file, 'rb') as f:
            status = gpg.decrypt_file(f, passphrase=str(dec_pass), output=fileName)

        print 'ok: ', status.ok
        print 'status: ', status.status
        print 'stderr: ', status.stderr
        print '...file written to '+fileName

if __name__ == "__main__":
    main(sys.argv[1:])
