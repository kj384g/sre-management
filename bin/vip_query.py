#!/usr/bin/python

#   written and tested by Kevin Jones using python 2.6.6
#
#   requires below imports, recommended to use pip to install modules
#   https://pip.pypa.io/en/latest/installing.html for more information
#
#   python-requests install
#   http://docs.python-requests.org/en/latest/user/install/

import os
import sys
import json
import getopt
import pprint
import requests
import tempfile
import time
import datetime

# disable ssl warning
requests.packages.urllib3.disable_warnings()

#   define api variables
url = 'http://pos2.np.wc1.yellowpages.com:9292'
path = '/vip/'
req_type = '.json'

#   define server time variables
ts = time.time()
servertime = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
serverhour = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d_%H%M%S')
serverdate = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d')

#   define pretty print with indent
#pp = pprint.PrettyPrinter(indent=2)

#   here we create a temporary file to store the initial json response
#   http://pymotw.com/2/tempfile/

#temp_file = tempfile.NamedTemporaryFile()

#   debugging to show the temp_file name and attributes after closing it
#try:
#    print 'temp:', temp_file
#    print 'temp.name:', temp_file.name
#finally:
#    temp_file.close()
#    print 'Exists after close:', os.path.exists(temp_file.name)

#   debugging to show arguments length and arguments string using sys

    #print 'Number of arguments:', len(sys.argv), 'arguments.'
    #print 'Argument List:', str(sys.argv)

#   here we define the command line switches for the arguments

def main(argv):
    environment = ''
    service = ''
    datacenter = ''
    port = ''
    healthcheck = False
    raw = False
    output = ''
    download = False
    dispmem = False
    try:
        opts, args = getopt.getopt(argv,"hcrme:s:d:p:o:",["environment=","service=","datacenter=","port=","output="])
    except getopt.GetoptError:
        print 'vipdb - command line json parse utility for "pos2" tool'
        print 'usage:'
        print '\t -e/--environment \t<environment>'
        print '\t -s/--service \t\t<service>'
        print '\t -d/--datacenter \t<datacenter>'
        print '\t -p/--port \t\t<port>'
        print '\t -o/--output\t\t</path/to/file>'
        print '\t -c\t\t\tprovides on fly vip healthcheck'
        print '\t -r\t\t\tdisplays raw json output'
        print '\t -m\t\t\tdisplays pool_members only'
        print '\t -h\t\t\tdisplays this help menu'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'vipdb - command line json parse utility for "pos2" tool'
            print 'usage:'
            print '\t -e/--environment \t<environment>'
            print '\t -s/--service \t\t<service>'
            print '\t -d/--datacenter \t<datacenter>'
            print '\t -p/--port \t\t<port>'
            print '\t -o/--output\t\t</path/to/file>'
            print '\t -c\t\t\tprovides on fly vip healthcheck'
            print '\t -r\t\t\tdisplays raw json output'
            print '\t -m\t\t\tdisplays pool_members only'
            print '\t -h\t\t\tdisplays this help menu'
            sys.exit()
        elif opt == ("-c"):
            healthcheck = True
        elif opt == ("-r"):
            raw = True
        elif opt == ("-m"):
            dispmem = True
        elif opt in ("-e", "--environment"):
            environment = arg
        elif opt in ("-s", "--service"):
            service = arg
        elif opt in ("-d", "--datacenter"):
            datacenter = arg
        elif opt in ("-p", "--port"):
            port = arg
        elif opt in ("-o", "--output"):
            output = arg
            download = True

#   here we add logic to properly define the wildcards and caret for the api

    if environment == '':
        environment = '*'
    elif environment == 'prod':
        environment = '^'
    elif environment == 'nca':
        environment = '^'
    else:
        environment = '^'+environment+"-"

    if service == '':
        service = '*'
    else:
        service = service+"*"

#   ensure the datacenter is valid else set to wildcard

    if datacenter == '':
        datacenter = '*'
    elif datacenter == 'ev1':
        datacenter = datacenter+"*"
    elif datacenter == 'wc1':
        datacenter = datacenter+"*"
    else:
        print 'unknown data center detected... setting to null'
        datacenter = '*'

#   ensure the port is an integer else set to wildcard

    if port.isalpha():
        print 'port is not an integer... setting to null'
        port = '*'
    elif port == '':
        port = '*'
    else:
        port = port+"*"

#   debugging to show each variables valus taken from the arguments

    #print 'environment is', environment
    #print 'service is', service
    #print 'datacenter is', datacenter
    #print 'port is', port
    #print 'output is', output

    uri = url+path+environment+service+datacenter+port+req_type

    print 'pulling data using', uri

    status = requests.get(uri)
    results = requests.get(uri).json()

    print 'http response:', status.status_code, ', server time is '+servertime

    if status.status_code == "404":
        print "sorry no results found."
        sys.exit()
    elif status.status_code == "504":
        print "gateway timeout occured."
        sys.exit()
    elif status.status_code == "500":
        print "internal server error occured!"
        sys.exit()
    else:
        success = 'true'

    if raw == True:
        print "processing...\n"
        print(json.dumps(results, indent=4))
        if download == True:
            with open(output, 'wb') as fd:
                for chunk in status.iter_content(1024):
                    fd.write(chunk)
                    fd.flush
                print 'writing results to...', output
                sys.exit()
        else:
            sys.exit()

    if download == True:
        with open(output, 'wb') as fd:
            for chunk in status.iter_content(1024):
                fd.write(chunk)
                fd.flush()
            print 'writing results to...', output
            sys.exit()
    else:
        print "processing...\n"

    if dispmem == True:
        for vip in results:
            if vip["timestamp"][0:9] == serverdate[0:9]:
                print vip["vip"]
                print "lb:", vip["lb_ip"]
                if 'pool_members' in vip["pool"]:
                    for member in vip["pool"]["pool_members"]:
                        print "- "+member["member"]
                else:
                    print "pool members is empty."
                    sys.exit()
            else:
                print "vip:", vip["vip"], "is out of date!"
                print "updated:", vip["timestamp"], '\n'
        sys.exit()

    for vip in results:
        if vip["timestamp"][0:9] == serverdate[0:9]:
            print "vip:", vip["vip"]
            if healthcheck == True:
                if vip["port"] == 443:
                    testip = 'https://'+vip["ip"]+'/health.txt'
                    print "health check:", requests.get(testip, timeout=5, verify=False).status_code
                else:
                    testip = 'http://'+vip["ip"]+'/health.txt'
                    print "health check:", requests.get(testip, timeout=5, verify=False).status_code
            else:
                quiet = 'shhhh'
            print "ip:", vip["ip"]
            print "state:", vip["state"]
            print "status:", vip["status"]
            print "lb_ip:", vip["lb_ip"]
            print "pool:", vip["pool"]["pool_name"]
            if 'pool_members' in vip["pool"]:
                for member in vip["pool"]["pool_members"]:
                    print "\tmember:", member["member"]
                    print "\thostname:", member["hostname"]
                    print "\tavailability_status:", member["availability_status"]
                    print "\tenabled_status:", member["enabled_status"]
            else:
                print "pool members is empty."
        else:
            print "vip:", vip["vip"], "is out of date!"
            print "updated:", vip["timestamp"], '\n'




#   pretty print the data
#   https://docs.python.org/2/library/pprint.html

    #pp.pprint(results).json()

if __name__ == "__main__":
    main(sys.argv[1:])
